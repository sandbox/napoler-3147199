<?php

namespace Drupal\terry_kg\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the terry_kg module.
 */
class KgControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "terry_kg KgController's controller functionality",
      'description' => 'Test Unit for module terry_kg and controller KgController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests terry_kg functionality.
   */
  public function testKgController() {
    // Check that the basic functions of module terry_kg.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
