<?php

/**
 * @file
 * Contains kg_entity.page.inc.
 *
 * Page callback for Kg entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Kg entity templates.
 *
 * Default template: kg_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_kg_entity(array &$variables) {
  // Fetch KgEntity Entity Object.
  $kg_entity = $variables['elements']['#kg_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
