<?php

namespace Drupal\terry_kg;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Kg entity entities.
 *
 * @ingroup terry_kg
 */
class KgEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['ner'] = $this->t('实体');
    $header['name'] = $this->t('描述');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\terry_kg\Entity\KgEntity $entity */
    $row['id'] = $entity->id();
    $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load( $entity->field_knowledge->target_id);
    // $row['ner'] =$term->name->value;
    $row['ner'] = Link::createFromRoute(
      $term->name->value,
      'terry_kg.kg_controller_id',
      ['id' => $entity->field_knowledge->target_id]
    );

    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.kg_entity.edit_form',
      ['kg_entity' => $entity->id()]
    );


    return $row + parent::buildRow($entity);
  }

}
