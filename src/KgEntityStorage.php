<?php

namespace Drupal\terry_kg;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\terry_kg\Entity\KgEntityInterface;

/**
 * Defines the storage handler class for Kg entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Kg entity entities.
 *
 * @ingroup terry_kg
 */
class KgEntityStorage extends SqlContentEntityStorage implements KgEntityStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(KgEntityInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {kg_entity_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {kg_entity_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(KgEntityInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {kg_entity_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('kg_entity_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
