<?php

namespace Drupal\terry_kg;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
// use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Theme\Registry;
use Drupal\Core\TypedData\TranslatableInterface;
// use Symfony\Component\DependencyInjection\ContainerInterface;
// use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Kg entity entities.
 *
 * @ingroup terry_kg
 */
class KgEntityViewBuilder extends EntityViewBuilder {


  /**
   * {@inheritdoc}
   */
  // public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
  //   $build=parent::view( $entity, $view_mode , $langcode);
  //   // $build["#kg_entity"]->values['name']['x-default']="牛逼的修改ia";
  //   dump($build['#kg_entity']->get("uuid")->getValue());
  //   dump($build['#kg_entity']->get("field_miaoshu")->getValue()['target_id']);
  //   dump($build);
    
  //   // dump(Element::children($entity));
  //   dump($entity);
  //   exit();
  //   return $build;
  // }
  // public function getBuildDefaults(EntityInterface $entity, $view_mode){
  //   $build=parent::build( $entity, $view_mode );
  //   return $build;

  // }

  public function viewField(FieldItemListInterface $items, $display_options = []) {
    $entity = $items->getEntity();
    if ($entity instanceof TranslatableInterface && $entity->isTranslatable()) {
      $entity = \Drupal::service('entity.repository')->getTranslationFromContext($entity);
    }
    $field_name = $items->getFieldDefinition()->getName();
    $display = $this->getSingleFieldDisplay($entity, $field_name, $display_options);

    $output = [];
    $build = $display->build($entity);
    if (isset($build[$field_name])) {
      $output = $build[$field_name];
    }

    return $output;
  }

}