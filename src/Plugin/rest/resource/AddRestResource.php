<?php

namespace Drupal\terry_kg\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Component\Utility;
use Drupal\taxonomy\Entity\Term;

// use Symfony\Component\HttpFoundation\Request;
/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "add_rest_resource",
 *   label = @Translation("提交接口"),
 *   uri_paths = {
 *     "create" = "/tkg/add"
 *   }
 * )
 */
class AddRestResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('terry_kg');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }
  /****
   * 
   * 添加术语
   * 
   * 
   */
    function addtid($term_value){
      $language =  \Drupal::languageManager()->getCurrentLanguage()->getId();
      // $term_value="柯基犬";
      if ($terms = taxonomy_term_load_multiple_by_name($term_value, 'knowledge')) {
        // Only use the first term returned; there should only be one anyways if we do this right.
        $term = reset($terms);
      } else {
        $term = \Drupal\taxonomy\Entity\Term::create([
          'name' => $term_value,
          'vid' => 'knowledge',
          'langcode' => $language,
        ]);
        $term->save();
      }
      $tid = $term->id();
      return $tid;
    }


/**
 * 
 * 检查实体是否存在
 * 
 * 
 * ***/
function checkkg($description,$tid){
  // $query = Drupal::service('entity.query')
  // ->get('kg_entity')
  // ->condition('type', 'description');
  // // ->condition('field_something', 'some_value');
  // $entity_ids = $query->execute();
  // // dump($entity_ids);
  // // exit();
  // return $entity_ids;
  $ids = \Drupal::entityQuery('kg_entity')
  // ->condition('status', 1)
  ->condition('type', 'description')
  ->condition('name', $description)
  ->condition('field_knowledge', $tid)
  ->execute();
  // return $ids;
  $ids=array_keys($ids);
  foreach ($ids as $key=>$v){ 

     if ($key==0){
      continue;
     }
    //  print_r($v);
    $this->auto_remove_duplicate($v);
     
  } 

    return $ids;
    

}
/**
 * 自动清理重重复的实体
 * 
 * 
 * **/
function auto_remove_duplicate($id){
  $entityStorage = \Drupal::entityTypeManager()->getStorage("kg_entity"); //获取节点实体储存处理器
  $aEntity = $entityStorage->load($id);
  $aEntity->delete();
  // Logs a notice

  \Drupal::logger('kg_entity')->info('@type: deleted %title.',
  array(
      '@type' => "kg_entity",
      '%title' => $aEntity->id(),
  ));
}

    /**
     * Responds to POST requests.
     *
     * @param string $payload
     *
     * @return \Drupal\rest\ModifiedResourceResponse
     *   The HTTP response object.
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     *   Throws exception expected.
     */
    public function post($payload) {
      // public function post(Request $request) {
      
        // You must to implement the logic of your REST Resource here.
        // Use current user after pass authentication to validate access.
        // dsm($this->currentUser);
        if (!$this->currentUser->hasPermission('access content')) {
            throw new AccessDeniedHttpException();
        }
           // 当前登录用户信息
      // $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        // dsm(\Drupal::request()->getMethod());
        // $payload["uid"]=$user;
        $payload["message"]="已经登录";
        // if  (is_null($payload["kg"])){
          if  ($payload["kg"]){
                            $tid=$this->addtid($payload["kg"]);
                            $entity_ids=$this->checkkg($payload["name"],$tid);
                            if (empty($entity_ids)){
                                                
                                #添加自定义的实体
                                $entityStorage = \Drupal::entityTypeManager()->getStorage("kg_entity"); //获取节点实体储存处理器
                                // $field_source_data=[];
                                // foreach ($array as $payload["field_source"])
                                //   {
                      
                                //   }


                                $aEntity = $entityStorage->create(
                                  ['name' => $payload["name"],
                                  'field_rank'=> $payload["field_rank"],
                                  'type' => 'description',
                                  'field_source'=> $payload["field_source"],
                                  // 'field_source'=>[
                                  //   ["uri"=>"internal:/see/rre34","title"=>"测试输入引用"],
                                  //   ["uri"=>"internal:/see/rre3ew","title"=>"测试输入引用1"]

                                  // ],
                                  'field_knowledge'=>[
                                    ["target_id"=>$tid]
                                  ]
                                  
                                  ]
                                
                                
                                ); //创建一篇文章（创建一个实体）
                                $aEntity->save(); //保存实体
                                // dump($aEntity->uuid());
                                // dump($aEntity);
                                $payload['uuid']=$aEntity->uuid();
                                $payload["entity_ids"]=$entity_ids;
                                $payload['message']="新建实体:".$aEntity->id();
                                \Drupal::logger('kg_entity')->info('@type: 更新 %title ( %id ).',
                                array(
                                    '@type' => "kg_entity",
                                    '%title' => $payload["kg"]+$payload["name"],
                                    '%id' => $aEntity->id(),
                                ));

                            }else{
                                  $payload["entity_ids"]=$entity_ids;
                                  $payload["message"]="已经存在:".$entity_ids[0];
                                  $entityStorage = \Drupal::entityTypeManager()->getStorage("kg_entity"); //获取节点实体储存处理器
                                  $aEntity = $entityStorage->load($entity_ids[0]);
                                  // print($aEntity);
                                  // exit();
                                  // $aEntity->get(field_rank)->va
                                if($payload["update"]=="True"){
                                  $payload["message"]="强制更新:".$entity_ids[0];
                                    $aEntity->field_rank=$payload["field_rank"];
                                    $aEntity->field_source=$payload["field_source"];
                                    $aEntity->save();


                                    \Drupal::logger('kg_entity')->info('@type: 强制更新 %kg %title ( %id ).',
                                    array(
                                        '@type' => "kg_entity",
                                        '%title' => $payload["name"],
                                        '%kg' =>$payload["kg"],
                                        '%id' => $entity_ids[0],
                                    ));
                                  }elseif ($aEntity->get('field_rank')->get(0)->getValue()['value']!=$payload["field_rank"]){
                                    $payload["message"]="更新field_rank:".$entity_ids[0];
                                    $aEntity->field_rank=$payload["field_rank"];
                                    $aEntity->field_source=$payload["field_source"];
                                    $aEntity->save();
                                    \Drupal::logger('kg_entity')->info('@type: 更新rank %kg %title ( %id ).',
                                    array(
                                        '@type' => "kg_entity",
                                        '%title' => $payload["name"],
                                        '%kg' =>$payload["kg"],
                                        '%id' => $entity_ids[0],
                                    ));
                                  }
                 


                            }
     




        }else{
          // $payload['nnn']='432ew234';

        }
        return new ModifiedResourceResponse($payload, 200);
        // ResourceResponse
        // return new ResourceResponse($payload);

    }

}
