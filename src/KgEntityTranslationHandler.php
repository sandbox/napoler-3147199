<?php

namespace Drupal\terry_kg;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for kg_entity.
 */
class KgEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
