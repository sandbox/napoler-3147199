<?php

namespace Drupal\terry_kg;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\terry_kg\Entity\KgEntityInterface;

/**
 * Defines the storage handler class for Kg entity entities.
 *
 * This extends the base storage class, adding required special handling for
 * Kg entity entities.
 *
 * @ingroup terry_kg
 */
interface KgEntityStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Kg entity revision IDs for a specific Kg entity.
   *
   * @param \Drupal\terry_kg\Entity\KgEntityInterface $entity
   *   The Kg entity entity.
   *
   * @return int[]
   *   Kg entity revision IDs (in ascending order).
   */
  public function revisionIds(KgEntityInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Kg entity author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Kg entity revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\terry_kg\Entity\KgEntityInterface $entity
   *   The Kg entity entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(KgEntityInterface $entity);

  /**
   * Unsets the language for all Kg entity with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}
