<?php

namespace Drupal\terry_kg\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Kg entity entities.
 *
 * @ingroup terry_kg
 */
class KgEntityDeleteForm extends ContentEntityDeleteForm {


}
