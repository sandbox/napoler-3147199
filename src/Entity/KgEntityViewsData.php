<?php

namespace Drupal\terry_kg\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Kg entity entities.
 */
class KgEntityViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    // dump($data);
    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
