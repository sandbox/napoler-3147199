<?php

namespace Drupal\terry_kg\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Kg entity type entity.
 *
 * @ConfigEntityType(
 *   id = "kg_entity_type",
 *   label = @Translation("Kg entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\terry_kg\KgEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\terry_kg\Form\KgEntityTypeForm",
 *       "edit" = "Drupal\terry_kg\Form\KgEntityTypeForm",
 *       "delete" = "Drupal\terry_kg\Form\KgEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\terry_kg\KgEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "kg_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "kg_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/kg_entity_type/{kg_entity_type}",
 *     "add-form" = "/admin/structure/kg_entity_type/add",
 *     "edit-form" = "/admin/structure/kg_entity_type/{kg_entity_type}/edit",
 *     "delete-form" = "/admin/structure/kg_entity_type/{kg_entity_type}/delete",
 *     "collection" = "/admin/structure/kg_entity_type"
 *   }
 * )
 */
class KgEntityType extends ConfigEntityBundleBase implements KgEntityTypeInterface {

  /**
   * The Kg entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Kg entity type label.
   *
   * @var string
   */
  protected $label;

}
