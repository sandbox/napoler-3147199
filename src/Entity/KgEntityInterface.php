<?php

namespace Drupal\terry_kg\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Kg entity entities.
 *
 * @ingroup terry_kg
 */
interface KgEntityInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Kg entity name.
   *
   * @return string
   *   Name of the Kg entity.
   */
  public function getName();

  /**
   * Sets the Kg entity name.
   *
   * @param string $name
   *   The Kg entity name.
   *
   * @return \Drupal\terry_kg\Entity\KgEntityInterface
   *   The called Kg entity entity.
   */
  public function setName($name);

  /**
   * Gets the Kg entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Kg entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Kg entity creation timestamp.
   *
   * @param int $timestamp
   *   The Kg entity creation timestamp.
   *
   * @return \Drupal\terry_kg\Entity\KgEntityInterface
   *   The called Kg entity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Kg entity revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Kg entity revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\terry_kg\Entity\KgEntityInterface
   *   The called Kg entity entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Kg entity revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Kg entity revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\terry_kg\Entity\KgEntityInterface
   *   The called Kg entity entity.
   */
  public function setRevisionUserId($uid);

}
