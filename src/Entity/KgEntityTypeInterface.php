<?php

namespace Drupal\terry_kg\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Kg entity type entities.
 */
interface KgEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
