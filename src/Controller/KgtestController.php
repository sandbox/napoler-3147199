<?php

namespace Drupal\terry_kg\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\taxonomy\Entity\Term;
/**
 * Class KgtestController.
 */
class KgtestController extends ControllerBase {

  function checkkg($description,$tid){
    // $query = Drupal::service('entity.query')
    // ->get('kg_entity')
    // ->condition('type', 'description');
    // // ->condition('field_something', 'some_value');
    // $entity_ids = $query->execute();
    // // dump($entity_ids);
    // // exit();
    // return $entity_ids;
    $ids = \Drupal::entityQuery('kg_entity')
    // ->condition('status', 1)
    ->condition('type', 'description')
    ->condition('name', $description)
    ->condition('field_knowledge', $tid)
    ->execute();
    // return $ids;
    $ids=array_keys($ids);


      foreach ($ids as $key=>$v){ 

       if ($key==0){
         
        continue;
       }
      //  print_r($v);
      $this->auto_remove_duplicate($v);
       
    } 

      return $ids;
      

  }
  /**
   * 自动清理重重复的实体
   * 
   * 
   * **/
  function auto_remove_duplicate($id){
    $entityStorage = \Drupal::entityTypeManager()->getStorage("kg_entity"); //获取节点实体储存处理器
    $aEntity = $entityStorage->load($id);
    $aEntity->delete();
    // Logs a notice

    \Drupal::logger('kg_entity')->info('@type: deleted %title.',
    array(
        '@type' => "kg_entity",
        '%title' => $aEntity->id(),
        // '%tid' =>$aEntity->id()
    ));
  }


  /**
   * Test.
   *
   * @return string
   *   Return Hello string.
   */
  public function test() {



    //  #根据uuid获取实体信息并修改
    // //  $uuid = "cb5d8faa-2d0f-4450-9c4b-d29060ee5c75";
    // //  $entity = \Drupal::entityManager()->loadEntityByUuid('kg_entity', $uuid);

    // //  dump( $entity);

    // //  $payload["entity_ids"]=$entity_ids;
    // //  $payload["message"]="updata:".$entity_ids;
    //  $entityStorage = \Drupal::entityTypeManager()->getStorage("kg_entity"); //获取节点实体储存处理器
    //  $aEntity = $entityStorage->load(31);
    //  print_r($aEntity);
    //   print_r($aEntity->get('field_rank')->get(0)->getValue()['value']);
    //  exit();
    //  $aEntity->field_rank=11;
    //  $aEntity->save();









    //  $language =  \Drupal::languageManager()->getCurrentLanguage()->getId();
    // $term_value="柯基犬";
    // if ($terms = taxonomy_term_load_multiple_by_name($term_value, 'knowledge')) {
    //   // Only use the first term returned; there should only be one anyways if we do this right.
    //   $term = reset($terms);
    // } else {
    //   $term = \Drupal\taxonomy\Entity\Term::create([
    //     'name' => $term_value,
    //     'vid' => 'knowledge',
    //     'langcode' => $language,
    //   ]);
    //   $term->save();
    // }
    // $tid = $term->id();
    // dump( $term);
    
    //   #添加自定义的实体
    //   $entityStorage = \Drupal::entityTypeManager()->getStorage("kg_entity"); //获取节点实体储存处理器
    //   $aEntity = $entityStorage->create(
        
    //     ['name' => "我的实体内容",
    //     'field_rank'=>2,
    //     'type' => 'description',
    //     'field_source'=>[
    //       ["uri"=>"internal:/see/rre34","title"=>"测试输入引用"],
    //       ["uri"=>"internal:/see/rre3ew","title"=>"测试输入引用1"]

    //     ],
    //     'field_knowledge'=>[
    //       ["target_id"=>$tid]
    //     ]
        
    //     ]
      
      
    //   ); //创建一篇文章（创建一个实体）
    //   $aEntity->save(); //保存实体

    //   dump($aEntity->uuid());
    //   dump($aEntity);
      $tid=[19];
      // $description='一种流行于全球的宠物犬';
      // // print("33");
      // print_r($this->checkkg($description,$tid));

      // $message=array('@type: deleted %title.',
      // array(
      //     '@type' => "php",
      //     '%title' => "ddd",
      // ));
      // \Drupal::logger('kg_entity')->info($message);
      // $this->auto_remove_duplicate(3);


      $entityStorage = \Drupal::entityTypeManager()->getStorage("kg_entity"); //获取节点实体储存处理器
      $aEntity = $entityStorage->load(4);
      print_r($aEntity->name->text);
      \Drupal::logger('kg_entity')->info('@type: deleted %title.',
      array(
          '@type' => "kg_entity",
          '%title' =>$aEntity->get('name')->get(0)->getValue()['value'],
          // '%tid' =>$aEntity->id()
      ));

      exit();

    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: test')
    ];
  }

}
