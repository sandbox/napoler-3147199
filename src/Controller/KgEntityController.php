<?php

namespace Drupal\terry_kg\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\terry_kg\Entity\KgEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class KgEntityController.
 *
 *  Returns responses for Kg entity routes.
 */
class KgEntityController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Displays a Kg entity revision.
   *
   * @param int $kg_entity_revision
   *   The Kg entity revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($kg_entity_revision) {
    $kg_entity = $this->entityTypeManager()->getStorage('kg_entity')
      ->loadRevision($kg_entity_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('kg_entity');

    return $view_builder->view($kg_entity);
  }

  /**
   * Page title callback for a Kg entity revision.
   *
   * @param int $kg_entity_revision
   *   The Kg entity revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($kg_entity_revision) {
    $kg_entity = $this->entityTypeManager()->getStorage('kg_entity')
      ->loadRevision($kg_entity_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $kg_entity->label(),
      '%date' => $this->dateFormatter->format($kg_entity->getRevisionCreationTime()),
    ]);
  }

  /**
   * Generates an overview table of older revisions of a Kg entity.
   *
   * @param \Drupal\terry_kg\Entity\KgEntityInterface $kg_entity
   *   A Kg entity object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(KgEntityInterface $kg_entity) {
    $account = $this->currentUser();
    $kg_entity_storage = $this->entityTypeManager()->getStorage('kg_entity');

    $langcode = $kg_entity->language()->getId();
    $langname = $kg_entity->language()->getName();
    $languages = $kg_entity->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $kg_entity->label()]) : $this->t('Revisions for %title', ['%title' => $kg_entity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all kg entity revisions") || $account->hasPermission('administer kg entity entities')));
    $delete_permission = (($account->hasPermission("delete all kg entity revisions") || $account->hasPermission('administer kg entity entities')));

    $rows = [];

    $vids = $kg_entity_storage->revisionIds($kg_entity);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\terry_kg\KgEntityInterface $revision */
      $revision = $kg_entity_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $kg_entity->getRevisionId()) {
          $link = $this->l($date, new Url('entity.kg_entity.revision', [
            'kg_entity' => $kg_entity->id(),
            'kg_entity_revision' => $vid,
          ]));
        }
        else {
          $link = $kg_entity->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.kg_entity.translation_revert', [
                'kg_entity' => $kg_entity->id(),
                'kg_entity_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('entity.kg_entity.revision_revert', [
                'kg_entity' => $kg_entity->id(),
                'kg_entity_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.kg_entity.revision_delete', [
                'kg_entity' => $kg_entity->id(),
                'kg_entity_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['kg_entity_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
