<?php

namespace Drupal\terry_kg\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Link;

/**
 * Class KgController.
 */
class KgController extends ControllerBase {

  /**
   * Id.
   *
   * @return string
   *   Return Hello string.
   */
  public function getContent($id,$page=NULL) {
    // return [
    //   '#type' => 'markup',
    //   // '#markup' => $this->t('Implement method: id with parameter(s): $id'),
    //   '#markup' => $this->t('zje')+$id,
    // ];

  if ($page==NULL){
    $page=0;
  }
  elseif($page==0){
    // drupal_set_header('Status: 404 Not Found');
    throw new NotFoundHttpException();
  }


// 引用views生成的列表

// Get the view machine id.
$view = \Drupal\views\Views::getView('knowledges');
// Set the display machine id.
$view->setDisplay('embed_tags');
$view->setArguments([$id]);
// 限制分页
$view->setItemsPerPage(5);
// Get the title.
// $title = $view->getTitle();
// Render.
$render = $view->render();

// $variables['content']['view_title'] = $the_title_render_array;
// Or $variables['content']['view_title']['#prefix'] = $title;.
$variables['content']['view_output_tags'] = $render;
// dsm($variables);



$Pager=30;

// 引用views生成的列表

// Get the view machine id.
$view = \Drupal\views\Views::getView('knowledges');
// Set the display machine id.
$view->setDisplay('embed');
$view->setArguments([$id]);
// $view->setOffset(2);
// $view->setLimit(3);
$view->setItemsPerPage($Pager);
$view->setOffset($Pager*$page);

// Get the title.
// $title = $view->getTitle();
// Render.
$render = $view->render();

// $the_title_render_array = [
//   '#markup' => t('@title', ['@title'=> $title]),
//   '#allowed_tags' => ['h2'],
// ];

// $variables['content']['view_title'] = $the_title_render_array;
// Or $variables['content']['view_title']['#prefix'] = $title;.
$variables['content']['view_output'] = $render;


// $variables['content']['view_output_nb'] = '$render';



$total_rows = count($view->result);
// $variables['content']['view_output_total_rows'] = $total_rows;
// dpm($total_rows, 'total_rows');
  // drupal_set_header('Status: 404 Not Found');
  // exit();

  // $variables['content']['view_output_edit'] = Link::createFromRoute(
  //   t("edit"),
  //   'entity.taxonomy_term.edit_form',
  //   ['id' => $id]
  // );






  if ($page<=1){
    $options = [
      'attributes' => [
        'class' => 'ui left attached button'
      ],
    ];
    $pre= Link::createFromRoute(
      t("第一页"),
      'terry_kg.kg_controller_id',
      ['id' => $id],
       $options
    );
  }else{

    $options = [
      'attributes' => [
        'class' => 'ui left attached button'
      ],
    ];
    $pre= Link::createFromRoute(
      t("上一页"),
      'terry_kg.kg_controller_id_page',
      ['id' => $id,'page' => $page-1],
       $options
    );
  }

  // dsm($next);


  $variables['content']['pre']  =  [
    '#type' => 'markup',
    // '#markup' => $this->t('Implement method: id with parameter(s): $id'),
    '#markup' => $pre->toString()
  ];












 if ($total_rows==$Pager){
  $options = [
    'attributes' => [
      'class' => 'right attached ui button'
    ],
  ];
  $next= Link::createFromRoute(
    t("下一页"),
    'terry_kg.kg_controller_id_page',
    ['id' => $id,'page' => $page+1],
     $options
  );

  $variables['content']['next']  = [
    '#type' => 'markup',
    '#markup' => $next->toString()
  ];


 }




  // dsm($variables);


  if ($total_rows<1){
    throw new NotFoundHttpException();
  }


      // #根据uuid获取实体信息

      // $entityStorage = \Drupal::entityManager()->getStorage('taxonomy_term');
      // $term=$entityStorage->load($id);
      
      // dsm($term->vid->value);
      // if ($term->vid->value=="knowledge"){

      // }else{
      //   return array(
      //     //Your theme hook name
      //     '#theme' => 'terry-kg-theme-hook',      
      //     '#content' => $variables,
      //   );
      // }

      // dsm($term);


    return array(
      //Your theme hook name
      '#theme' => 'terry-kg-theme-hook',      
      //Your variables
      '#content' =>$variables,
      // '#next' => $next,
      // '#variable3' => $myArray,
    );

  }


  
  /**
   * Id.
   *
   * @return string
   *   Return Hello string.
   */
  public function getTitle($id,$page=0) {
    $entityStorage = \Drupal::entityManager()->getStorage('taxonomy_term');
    $term=$entityStorage->load($id);
    $title = $term->name->value;
    // if ($page>0){
    //   return $title+$page;
    // }else{
    //   return $title;
    // }
    return $title;


  }

}
